# Angular Starter + Home

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.6.

The default selector prefix for this project is 'app'. To change the prefix, you will need to:

* Update the selector in app.component.ts and index.html

* Update the selector in home.component.ts and page-not-found.component.ts

* Update the "prefix" setting in .angular-cli.json, as well as the "directive-selector" and "component-selector" settings in tslint.json.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Start

Run `start` for a dev server with the `--hmr` and `--open` flags enabled. The project will automatically open `http://localhost:4200/` in your browser.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Build for production

Run `ng build:prod` to build the project with the flag `--prod` enabled. The build artifacts will be stored in the `dist/` directory.

## Lint TypeScript

Run `lint` to execute tsLint with semistandard code style rules.

## Running unit tests

Run `test` to execute the unit tests via [Jest](https://facebook.github.io/jest/).

## Running unit tests in watch mode

Run `test:watch` to execute the unit tests via [Jest](https://facebook.github.io/jest/) with watch mode enabled.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
