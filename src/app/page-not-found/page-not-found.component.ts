import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss']
})
export class PageNotFoundComponent implements OnInit {
  title: string;
  description: string;

  constructor(
    private titleService: Title
  ) { }

  ngOnInit() {
    this.titleService.setTitle('Angular Starter | 404');
    this.title = this.titleService.getTitle().split(' | ')[1] + ' Page Not Found';
    this.description = 'The page you are looking for cannot be found.';
  }
}
